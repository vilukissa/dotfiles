AUDIODEVICE="alsa_output.usb"
xrandr --output HDMI-0 --mode 2560x1440 --rate 144 --output DVI-D-0 --mode 1920x1080 --rate 144 --left-of HDMI-0 --output HDMI-1 --mode 1920x1080 --right-of HDMI-0 

PANAME=$(pactl list | grep -A2 'Source #' | grep 'Name: ' | cut -d" " -f2 | grep -e $AUDIODEVICE | sed s/."monitor"//)
echo $PANAME
pacmd set-default-sink $PANAME
