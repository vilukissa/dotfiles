#!/usr/bin/env
import yeelight as yee
import time
import sys
import json
import os.path

# RUN AS GNOME KB-SHORTCUT: bash -c 'python ~/dotfiles/scripts/light.py --all --toggle'

# DEFAULTS
DEF_TEMP = 2500
DEF_BRIGHT = 100
STEP_SIZE = 20
TEMP_STEP = 500
COLOR_STEP = 30
DATA_PATH = "/home/vaino/bulbs.json"

# RGB BULBS
def setBulbColor(bulb, R, G, B, brightness=DEF_BRIGHT):
    yee.Bulb(bulb["ip"]).set_rgb(R, G ,B)
    yee.Bulb(bulb["ip"]).set_brightness(brightness)

def setAllBulbsColor(bulbs, R, G, B, brightness=DEF_BRIGHT):
    turnBulbsOn(bulbs)
    for bulb in bulbs:
        setBulbColor(bulb, R, G, B, brightness)

## WHITE BULBS
def setBulbWhite(bulb, brightness=DEF_BRIGHT, temp=DEF_TEMP):
    yee.Bulb(bulb["ip"]).set_color_temp(temp)
    yee.Bulb(bulb["ip"]).set_brightness(brightness)

def setAllBulbsWhite(bulbs, brightness=DEF_BRIGHT, temp=DEF_TEMP):
    turnBulbsOn(bulbs)
    for bulb in bulbs:
        setBulbWhite(bulb, brightness, temp)


# CHANGE BRIGHTNESS
def increaseBrightness(bulb):
    B = yee.Bulb(bulb["ip"])
    currentBr = int(B.get_properties()['current_brightness'])
    B.set_brightness(currentBr + STEP_SIZE)

def increaseBrightnessAll(bulbs):
    for bulb in bulbs:
        increaseBrightness(bulb)

def decreaseBrightness(bulb):
    B = yee.Bulb(bulb["ip"])
    currentBr = int(B.get_properties()['current_brightness'])
    B.set_brightness(currentBr - STEP_SIZE)

def decreaseBrightnessAll(bulbs):
    for bulb in bulbs:
        decreaseBrightness(bulb)


# CHANGE COLOR TEMPERATURE
def increaseCT(bulb):
    B = yee.Bulb(bulb["ip"])
    currentBr = int(B.get_properties()['ct'])
    B.set_color_temp(currentBr + TEMP_STEP)

def increaseCTAll(bulbs):
    for bulb in bulbs:
        increaseCT(bulb)

def decreaseCT(bulb):
    B = yee.Bulb(bulb["ip"])
    currentBr = int(B.get_properties()['ct'])
    B.set_color_temp(currentBr - TEMP_STEP)

def decreaseCTAll(bulbs):
    for bulb in bulbs:
        decreaseCT(bulb)


# CHANGE COLOR
def increaseColor(bulb):
    B = yee.Bulb(bulb["ip"])
    hue = int(B.get_properties()['hue'])
    print("Hue", hue)
    if (hue > 329):
        hue = 0
    else:
        hue = hue + COLOR_STEP
    B.set_hsv((hue), 100)

def increaseColorAll(bulbs):
    for bulb in bulbs:
        increaseColor(bulb)

def decreaseColor(bulb):
    B = yee.Bulb(bulb["ip"])
    hue = int(B.get_properties()['hue'])
    print("Hue", hue)
    if (hue < 30):
        hue = 359
    else:
        hue = hue - COLOR_STEP
    B.set_hsv((hue), 100)

def decreaseColorAll(bulbs):
    for bulb in bulbs:
        decreaseColor(bulb)




## BULBS ON/OFF
def turnBulbsOn(bulbs):
    for bulb in bulbs:
        yee.Bulb(bulb["ip"]).turn_on()
    return 1

def turnBulbsOff(bulbs):
    for bulb in bulbs:
        yee.Bulb(bulb["ip"]).turn_off()
    return 1

def toggleBulbs(bulbs):
    for bulb in bulbs:
        yee.Bulb(bulb["ip"]).toggle()
    return 1

## LOGIC

def findBulb(AllBulbs, id):
    for bulb in AllBulbs:
        if (bulb["ip"] == id):
            return bulb

def readData():
    # Check if file exists
    if(os.path.isfile(DATA_PATH)):
        print("File found")
        with open(DATA_PATH) as json_file:
            data = json.load(json_file)
        return data
    else:
        print("File not found")
        return []

def writeData(bulbs):
    with open(DATA_PATH, 'w') as json_file:
        json.dump(bulbs, json_file)
    print("Data written")

def setup():
    # Check if file exists
    fileData = readData()
    if (len(fileData) > 0):
        bulbs = fileData
        print("Data read from file")
    else:
        bulbs = yee.discover_bulbs()
        writeData(bulbs)
    return bulbs

def main(args):
    AllBulbs = setup()
    print(AllBulbs)
    brightess = DEF_BRIGHT
    temp = DEF_TEMP


    # Apply command to all bulbs
    if ("--all" in args):
        bulbs = AllBulbs

    # Apply command to certain bulbs
    elif ("-b" in args):
        i = args.index("-b")
        j = 1
        bulbs = []
        while(True):
            current = args[i + j]
            if ("--" in current): # Break on command
                break;
            bulbs.append(findBulb(AllBulbs, current))
            j = j + 1
            if ((i + j) == len(args)):
                break;
    else:
        print("Please specify bulb(s)")

    if ("--update" in args):
        bulbs = yee.discover_bulbs()
        writeData(bulbs)

    if ("--toggle" in args):
        toggleBulbs(bulbs)

    if ("--turnoff" in args):
        turnBulbsOff(bulbs)

    if ("--turnon" in args):
        turnBulbsOn(bulbs)

    if ("--increaseBrightness" in args):
        increaseBrightnessAll(bulbs)

    if ("--decreaseBrightness" in args):
        decreaseBrightnessAll(bulbs)

    if ("--increaseCT" in args):
        increaseCTAll(bulbs)

    if ("--decreaseCT" in args):
        decreaseCTAll(bulbs)

    if ("--increaseColor" in args):
        increaseColorAll(bulbs)

    if ("--decreaseColor" in args):
        decreaseColorAll(bulbs)


    if ("--setWhite" in args):
        i = args.index("--setWhite")
        if(i + 1  < len(args)):
            brightess = int(args[i + 1])
        if(i + 2 < len(args)):
            temp = int(args[i + 2])
        setAllBulbsWhite(bulbs, brightess, temp)

    if ("--setColor" in args):
        i = args.index("--setColor")
        if (len(args) < i + 3):
            print("Please specify R G B values")
            return
        R = int(args[i + 1])
        G = int(args[i + 2])
        B = int(args[i + 3])
        if (i + 4 < len(args)):
            brightess = int(args[i + 4])
        setAllBulbsColor(bulbs, R, G, B, brightess)


    return 1

main(sys.argv)
