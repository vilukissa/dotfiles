RESOLUTION=1920x1080
AUDIODEVICE="hdmi-stereo"
xrandr --listmonitors | sed -n '1!p' | sed -e 's/\s[0-9].*\s\([a-zA-Z0-9\-]*\)$/\1/g' | xargs -n 1 -- bash -xc 'xrandr --output $0 --mode '$RESOLUTION' --pos 0x0 --rotate normal'

PANAME=$(pactl list | grep -A2 'Source #' | grep 'Name: ' | cut -d" " -f2 | grep -e $AUDIODEVICE | sed s/."monitor"//)
echo $PANAME
pacmd set-default-sink $PANAME


